-- sperrgebiet 2022
-- Unlike others I see the meaning in mods that others can learn, expand and improve them. So feel free to use this in your own mods, 
-- add stuff to it, improve it. Your own creativity is the limit ;) If you want to mention me in the credits fine. If not, I'll live happily anyway :P
-- Yeah, I know. I should do a better job in document my code... Next time, I promise... 

SecondFillTrigger = {}

SecondFillTrigger.specName = string.format("spec_%s.secondFillTrigger", g_currentModName)

function SecondFillTrigger:initSpecialization()
	local schema = Vehicle.xmlSchema
	schema:setXMLSpecializationType("secondFillTrigger")

	schema:register(XMLValueType.NODE_INDEX, "vehicle.fillTriggerVehicle2#triggerNode", "")
	schema:register(XMLValueType.INT, "vehicle.fillTriggerVehicle2#fillUnitIndex", "")
	schema:register(XMLValueType.FLOAT, "vehicle.fillTriggerVehicle2#litersPerSecond", "")
	schema:setXMLSpecializationType()
end

function SecondFillTrigger.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(FillUnit, specializations)
end

function SecondFillTrigger.registerEventListeners(vehicleType)
		SpecializationUtil.registerEventListener(vehicleType, "onLoad", 		SecondFillTrigger)
		SpecializationUtil.registerEventListener(vehicleType, "onDelete", 		SecondFillTrigger)
		SpecializationUtil.registerEventListener(vehicleType, "onReadStream", 	SecondFillTrigger)
end

function SecondFillTrigger:onLoad(savegame)
    local spec = self[SecondFillTrigger.specName]
	local triggerNode = self.xmlFile:getValue("vehicle.fillTriggerVehicle2#triggerNode", nil, self.components, self.i3dMappings)
	
    if triggerNode ~= nil then
        spec.fillUnitIndex2 = Utils.getNoNil(self.xmlFile:getValue("vehicle.fillTriggerVehicle2#fillUnitIndex"), 1)
        spec.litersPerSecond2 = Utils.getNoNil(self.xmlFile:getValue("vehicle.fillTriggerVehicle2#litersPerSecond"), 50)
        spec.fillTrigger2 = FillTrigger.new(triggerNode, self, spec.fillUnitIndex2, spec.litersPerSecond2)

        if self:getPropertyState() ~= Vehicle.PROPERTY_STATE_SHOP_CONFIG then
            -- spec.fillTrigger2:finalize()
			-- We can't use the build-in function, as it will throw LUA errors
			SecondFillTrigger.finalizeTrigger(spec.fillTrigger2, self)
        end
    end
end

function SecondFillTrigger:onDelete()
    local spec = self[SecondFillTrigger.specName]
    if spec.fillTrigger2 ~= nil then
        spec.fillTrigger2:delete()
        spec.fillTrigger2 = nil
    end
end

function SecondFillTrigger.finalizeTrigger(trigger, vehicle)
	--self.moneyChangeType = 	{
	--						id = 37,
	--						statistic = "other",
	--						title = "finance_purchaseFuel"
	--						}
							
	trigger.moneyChangeType = vehicle.spec_fillTriggerVehicle.fillTrigger.moneyChangeType
end

function SecondFillTrigger:onReadStream(streamId, connection)
	local spec = self[SecondFillTrigger.specName]
	SecondFillTrigger.finalizeTrigger(spec.fillTrigger2, self)
end